<treqs>
<treqs-element id="e770de36920911eb9355f018989356c1" type="non-existing-type">

### 5-test-1 TReqs check shall detect incorrect types.

TReqs check should detect when a type is not listed in the ttim. In this case "non-existing-type" is not declared in the sample ttim.
</treqs-element>

<treqs-element id="2c600896920a11ebbb6ff018989356c1" type="requirement">

### 5-test-2 TReqs check shall detect incorrect link types.

TReqs check should detect when a link type is not listed in the ttim for the element it is contained in. In this case "relatesToo" is not declared in the sample ttim for elements of type requirement.
<treqs-link type="relatesToo" target="e770de36920911eb9355f018989356c1" />
</treqs-element>
<treqs-element id="56cbd2e0920a11ebb9d1f018989356c1" type="requirement">

### 5-test-3 Treqs check shall detect incorrect link types, even if they exist for other types.

TReqs check should detect when a link type is not listed in the ttim for the element it is contained in, even if it exists for another type. In this case "tests" is not declared in the sample ttim for elements of type requirement, but it exists for elements of type unittest.
<treqs-link type="tests" target="e770de36920911eb9355f018989356c1" />
</treqs-element>
<treqs-element type="requirement">

### 5-test-4 Treqs check shall detect missing IDs in elements.

TReqs check should detect when an id is missing from an element. In this case, the id argument is missing from the requirement.
</treqs-element>
<treqs-element id="940f4d62920a11eba034f018989356c1" type="requirement">

### 5-test-5 Treqs check shall detect missing target IDs in links.
TReqs check should detect when a target id is missing from a link element. In this case, the target argument is missing from the relatesTo link.
<treqs-link type="relatesTo" />
</treqs-element>
<treqs-element id="940f4d62920a11eba034f018989356c1" type="requirement">

### 5-test-6 Treqs check shall detect duplicate IDs in elements.
TReqs check should detect when there exists a duplicate id in treqs elements. 
</treqs-element>
<treqs-element id="c5402c1a919411eb8311f018989356c1" type="requirement">

### 5-test-7 Treqs check shall detect duplicate IDs in elements across files.
TReqs check should detect when there exists a duplicate id in treqs elements located in different files. In this case, the id of this requirement is identical to the id of 5.0 Parameters and default output of treqs check in file ./requirements/5-check-treqs.md.
</treqs-element>
<treqs-element id="4f5bcadad45711eb9de4f018989356c1" type="requirement">

### 5-test-8 Treqs check shall detect if an element referenced from a link does not exist.
In this case, there is no element with id "indexistent_target_id".
<treqs-link type="relatesTo" target="inexistent_target_id"/>
</treqs-element>
<treqs-element id="3cabd686d45811ebaaeef018989356c1" type="requirement">

### 5-test-9 Treqs check shall detect if a link with a defined target type references an existing element with another type.
In this case, the 'addresses' link needs to point to an element with type stakeholder-need, but references a requirement instead.
<treqs-link type="addresses" target="4f5bcadad45711eb9de4f018989356c1"/>
</treqs-element>

<treqs-element id="9d3a98c80bd711ec8e3ff018989356c1" type="">

### 5-test-10 Treqs check shall detect empty types.
In this case, the 'type' argument is empty in the treqs element. This should be detected.
</treqs-element>

</treqs>