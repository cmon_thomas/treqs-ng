# SensIT AR BIM Viewer

## Versions and Authors

| Date | Version | Comment | Author |
| --- | --- | --- | --- | 
| 2021-Apr-07 | 1.0 | Version submitted as part of Bachelor's thesis  | Ali Aziz (gusaziala@student.gu.se) and Lema Rassul (gusraslea@student.gu.se) |
| 2021-Sep-06 | 1.1 | Refined and made compatible with treqs | Eric Knauss (eric.knauss@cse.gu.se) |

Note: We are using the [treqs open source tool](https://gitlab.com/treqs-on-git) to manage traceability in this document.

## 1. Description


This document describes requirements for the SensIT AR BIM App, which aims to support the inspection of constructions (such as bridges). 
The SensIT AR BIM App should integrate with the SensIT Bim viewer, a web based app that allows to manage building information models (BIM) together with sensor data generated from sensors that are embedded in the constructs.
In particular, SensIT aims to use such sensor data to predict damages, such as cracks. 
While the SensIT BIM viewer supports reasoning about the general state of a set of constructs (e.g. bridges), the SensIT AR BIM App supports on premise, in situ work, e.g. when assessing the quality. 
It could guide an assessor to the hotspots of a bridge, where based on the SensIT machine learning a crack could be most likely observed. 


### 1.1 Goal and Scope

The goal of the software is to visualize the prediction of any cracks or defects in the structure by connecting mobile, AR enabled devices with the SensIT BIM viewer infrastructure. 
The context diagram in Figure 1 shows the AR viewer in its context as well as the interfaces to users and other systems that it must support.

> PlantUML code shown only for demonstration. Ideally, the code would be commented out and the UML diagram it generates would be shown.
> PlantUML does not directly support Context Diagrams. There are workarounds, but the result in more complicated code. 
```
@startuml sensit-context

actor client
actor contractor

  component bim as "BIM Database"
  component sens as "Sensor Data"
  component ml as "ML Predictions"

storage "Inspection of construction" #palegreen;line.dashed{
  component ar as "AR Viewer"

 
  inspector <<->> ar :(e1) view, (e2) find hotspots, (e3) annotate
  bim <<-->> contractor:maintain
  client <<->> ar :(e1) view, (e3) annotate
  client <<->> bim:maintain

  ar <<==>> bim:(e4) overlay, \nadd annotation
  ar <<== sens
  ar <<== ml
 
}
@enduml 
```
![sensit-ar-usecases](gen/sensit-context.png)

**Figure 1.** Context diagram of AR Viewer.

```
Legend:
- We use storage with green background and dashed line to show the domain
- We use arrows to show interaction and the text to denote the key events
```

Withing this scope, SensIT AR aims to reach the following business goals:

<treqs-element id="cd897ef221c811ec9bddc4b301c00591" type="information">

#### 1.1.1 Phases

The SensIT AR BIM App is embedded in the following high-level process of modern construction. 

1. Design, Client and Contractor agree on process and design of the construction. Sensors and models are decided on.
1. Production, Contractor produces the construction. AR can help to monitor the production.
1. Warranty phase, Contractor still responsible for maintenance. AR can help to assess the constructions and identify potential problems. 

</treqs-element>

#### 1.1.2 Business Case

> Omitting irrelevant data for test

#### 1.3.2 Overview of Functions in SensIT AR BIM viewer

```
@startuml sensit-ar-usecases
left to right direction
actor user
user <|- inspector

rectangle "SensIT AR BIM Viewer" {
  "view sensor data / \n prediction" as (view)
  "monitor structure" as (monitor)
  "take notes" as (notes)
  (login)

  user -- (view)
  user -- (monitor)
  user -- (notes)

  (view) ..> (login) : <<include>>
  (monitor) ..> (login) : <<include>>
  (notes) ..> (login) : <<include>>

  (upload picture) .> (notes) : <<extends>>  
}
@enduml 
```
![sensit-ar-usecases](gen/sensit-ar-usecases.png)

**Figure 2.** Use case diagram with core functionality of the AR Viewer.

The main use cases for the AR visualisation tool are presented in the use case diagram in Figure 2. The SensIT AR BIM Viewer is set not only to visualize areas of interest to inspectors during inspections, but also allows the user to review sensor data displayed in plots both before and during the inspection. The inspector is then also able to create annotations during the insepction to review the location and comments when returning back to the office. Previous notes from other inspectors can also be reviewed to make an assessment of any changes or causes for any discovered damage. 
